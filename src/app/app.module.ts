import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';

import { HttpClientModule } from '@angular/common/http';
import { DataService } from './services/data.service';
import { PagerService } from './services/pager.service';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [DataService, PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
