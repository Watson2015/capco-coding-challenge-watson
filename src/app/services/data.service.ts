import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {IDataPoint} from './dataPoint';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/* Used to retrive data from a json file and have it available for all components */
export class DataService {

  private _url = 'assets/data/sample_data.json';
  constructor(private http: HttpClient) { }


  getData(): Observable <IDataPoint[]> {
    return this.http.get<IDataPoint[]>(this._url);
  }
}
