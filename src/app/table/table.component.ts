import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { PagerService } from '../services/pager.service';


@Component({
  selector: '.app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  public data = [];
  pager: any = {};
  pagedItems: any[];

  constructor(private _dataService: DataService, private pagerService: PagerService) { }

  // Retrieves all data and initalizes the pagination
  ngOnInit() {
    this._dataService.getData()
    .subscribe(data => {
      this.data = data;
      this.setPage(1 , 10);
    });
  }
  // Used to fetch user input value to adjust rows of data scene 
  fetchRowSelector() {
    let x = (<HTMLInputElement>document.getElementById('batman')).value;
    if (x === '') {
      x = '10';
    }
    this.setPage(this.pager.currentPage, Number(x));
  }
  //Calls pagination service to render the appropriate rows in the table
  setPage(currentPage: number, rowSelected: number) {
    this.pager = this.pagerService.getPager(this.data.length, currentPage, rowSelected);
    this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  submit(id, status) {
    // This function would be a post to a WebServer. The sedo code would look like:
    /* return this.http.post<Article>('urlTOServer",{
      id: id,
      status, status
    });*/
    // For now we will alert for confirmation.
    alert('Are you sure you would like to submit the row selected, id:' + id + ' ,which has a status of: ' + status + ' ?');
  }
}
